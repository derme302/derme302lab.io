---
id: 30
title: 'The Glass is Falling - Update 1.1'
date: 2013-01-22T05:33:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2013/the-glass-is-falling-update-1-1/
permalink: /2013/01/the-glass-is-falling-update-1-1/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2013/01/the-glass-is-falling-update-11.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/6909146842437466288
categories:
  - GameMaker
  - Games
---
<div style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="http://derme.coffee/uploads/2013/01/r12-20-1-2013-menu.png"><img src="http://derme.coffee/uploads/2013/01/r12-20-1-2013-menu-225x300.png" alt="" width="240" height="320" border="0" /></a>
</div>

About this time last month I released &#8220;The Glass is Falling&#8221; via the <a href="http://steamcommunity.com/sharedfiles/filedetails/?id=115103698" target="_blank">Steam Workshop</a> and I received some really good feedback about the game. All of this has gone into creating the update which I uploaded yesterday.

<div>
</div>

<div>
  The main points of the update are:
</div>

<div>
</div>

  * New background and sprites
  * Physics Improvements
  * Various Bug Fixes

<div>
</div>

<!--more-->

<div>
</div>

<div>
</div>

The HTML5 port of the game is almost ready to be released, I just have to squash one last bug with the Physics engine. I was planning an initially release on CrySet Games, but they have since shut down and I'm now looking for other places to publish it.

<img class="aligncenter" src="http://derme.coffee/uploads/2013/01/r11_1-7-13_2-225x300.png" alt="" width="240" height="320" border="0" /> 

&nbsp;

Over the next month or so I'm going to be getting the game ready for a release on Google Play, I'm almost ready but I got a few things left to do:

  * Banner ads for the free version of the game
  * Online Highscore integration
  * Help screen included in game
  * Sound effects, though this may have to wait for a later update

<div style="clear: both; text-align: center;">
</div>