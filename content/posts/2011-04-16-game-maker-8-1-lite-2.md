---
id: 38
title: Game Maker 8.1 Lite
date: 2011-04-16T08:13:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2011/game-maker-8-1-lite-2/
permalink: /2011/04/game-maker-8-1-lite-2/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2011/04/game-maker-81-lite.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/6807317219228782835
image: /uploads/2014/05/GM_8.1_storefront_716x215-624x187.png
categories:
  - GameMaker
---
GameMaker 8.1 (Single word now) has received mixed reviews on the [Game Maker Community](http://gmc.yoyogames.com/), while a lot of members have given it thumbs up, many are upset that it has broken their code and is not compatible with certain extensions/DLLs. Personally though not only does it run all my games perfectly, it also runs them a lot faster than <span style="font-style: italic;">Game Maker</span> 8.0 (Two Words).

[YoYo Games](http://www.yoyogames.com/) made lots of noise about the new zoom feature in the room editor, while this is great (bit buggy though), the big thing new feature for me has to be the upgraded font rendering! Now all my games have sharp as crystal text. When Mike Dailly first brought up the whole font improvement, I just didn't get it, I couldn't even tell the difference between the two images he put up comparing the new and the old. But now I get it and I am going to rush out and buy the standard edition tomorrow (Literally).

<!--more-->

The second thing that I was personally looking forward to was the ability to name execute code statements. Like this;

[<img id="BLOGGER_PHOTO_ID_5596143302416343986" style="cursor: pointer; float: left; height: 24px; margin: 0 10px 10px 0; width: 24px;" src="http://4.bp.blogspot.com/-LQAsvFk98fc/Tal-rxS1I7I/AAAAAAAAABk/n0Q0d1sWwL4/s320/GM069.gif" alt="" border="0" />](http://4.bp.blogspot.com/-LQAsvFk98fc/Tal-rxS1I7I/AAAAAAAAABk/n0Q0d1sWwL4/s1600/GM069.gif)Execute a piece of code

&#8212;-Becomes&#8212;-

[<img id="BLOGGER_PHOTO_ID_5596143302416343986" style="cursor: pointer; float: left; height: 24px; margin: 0 10px 10px 0; width: 24px;" src="http://4.bp.blogspot.com/-LQAsvFk98fc/Tal-rxS1I7I/AAAAAAAAABk/n0Q0d1sWwL4/s320/GM069.gif" alt="" border="0" />](http://4.bp.blogspot.com/-LQAsvFk98fc/Tal-rxS1I7I/AAAAAAAAABk/n0Q0d1sWwL4/s1600/GM069.gif)Draw Menu

Simply by adding a single comment line at the start of the code.

> <pre>///Draw the menu
//rest of your code here</pre>

Cool, Huh? So this makes you life much easier when your trouble shooting.

Indenting code with the TAB key really makes it that much quicker to indent a selection of code, a feature which is just awesome.

Finally the new zoom function in the room editor, while one of the more noticeable changes they have made to GameMaker, I really didn't feel that excited about it. However now that I have used it, I'll never go back to GM 8.0.

So that’s it for GM Lite, overall I'd give it 4.5/5 Neurons, while it has improved GameMaker greatly , the release felt &#8220;rushed&#8221; and there was a couple of really obvious bugs. I sure though with the new Auto-Updater these errors will be fixed over the coming days.

Till next time folks!

<span style="font-size: 85%; font-style: italic;">Photo (GameMaker 8.1): </span>[<span style="font-size: 85%; font-style: italic;">YoYoGames Glog</span>](http://glog.yoyogames.com/)