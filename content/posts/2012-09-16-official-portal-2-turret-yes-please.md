---
id: 32
title: Official Portal 2 Turret? Yes please.
date: 2012-09-16T03:00:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2012/official-portal-2-turret-yes-please/
permalink: /2012/09/official-portal-2-turret-yes-please/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2012/09/official-portal-2-turret-yes-please.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/5260068899251681629
categories:
  - General
---
<div align="center">
  <div style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="http://derme.coffee/uploads/2012/09/valve-portal-2-sentry-turret-teaser.jpg"><img src="http://derme.coffee/uploads/2012/09/valve-portal-2-sentry-turret-teaser.jpg" alt="" border="0" /></a>
  </div>
  
  <p>
    &nbsp;
  </p>
</div>

Portal would have to be one of my favourite puzzle games of all time the others being Lemmings and Portal 2.

When Velociraptor proofing my place of dwelling, something I always wanted to include was a couple of Portal turrets, so I'm really hoping that Valve plan to start selling them on the adjacent self to the Weighted Companion Cubes.

<span style="font-size: x-small;">Image (Portal Turret): <a href="https://www.facebook.com/TheValveStore">Valve Store Facebook Page</a></span>