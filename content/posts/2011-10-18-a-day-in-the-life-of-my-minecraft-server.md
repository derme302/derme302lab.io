---
id: 34
title: A Day in the Life of my Minecraft Server
date: 2011-10-18T11:19:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2011/a-day-in-the-life-of-my-minecraft-server/
permalink: /2011/10/a-day-in-the-life-of-my-minecraft-server/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2011/10/day-in-life-of-my-minecraft-server.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/6981194356734455298
categories:
  - Minecraft
---
I've never been a big fan of Minecraft, and I probably will never be, which is a little bit strange seeing as I run a Minecraft server. The game just never clicked for me, I always found it a bit, dull. Though I will admit I'm a little bit jealous when people go out and build some incredible stuff like a 8-bit computer or a 3d printer.

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Minecraft_Destiny.png"><img src="http://derme.coffee/uploads/2011/10/Minecraft_Destiny-300x169.png" alt="" width="320" height="180" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Destiny out of Stargate Universe - By Blade695
    </td>
  </tr>
</table>

<div style="clear: both; text-align: center;">
</div>

Watching that ship getting constructed was absolutely epic, it even has a gate on it! Only thing it doesn't do is jump to FTL.

<div style="clear: both; text-align: center;">
</div>

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Minecraft_StarDestroyer.png"><img src="http://derme.coffee/uploads/2011/10/Minecraft_StarDestroyer-300x170.png" alt="" width="320" height="182" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Star Destroyer out of Star Wars - By blade695
    </td>
  </tr>
</table>

While I prefer the Destiny, you can't forget the good old Star Destroyer, it brings back memories.

<!--more-->

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Mincraft_Farm.png"><img src="http://derme.coffee/uploads/2011/10/Mincraft_Farm-300x168.png" alt="" width="320" height="179" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Farm - By Cyphermonkey
    </td>
  </tr>
</table>

Even with all the spaceships being built, some people can't leave the simple life behind.

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Minecraft_MonsterHouse.png"><img src="http://derme.coffee/uploads/2011/10/Minecraft_MonsterHouse-300x169.png" alt="" width="320" height="180" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Monster House - By Cyphermonkey
    </td>
  </tr>
</table>

Even though I like this house, it reminds me of that really bad movie that looked more like cut scene out of a computer game than a film.

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Minecraft_Resort.png"><img src="http://derme.coffee/uploads/2011/10/Minecraft_Resort-300x168.png" alt="" width="320" height="179" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Resort - Water Thing
    </td>
  </tr>
</table>

When you get the chance to build anything you want, some people create strange things, personally I think this person played way to much RollerCoaster Tycoon when they were growing up.

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Mincraft_LordCastle.png"><img src="http://derme.coffee/uploads/2011/10/Mincraft_LordCastle-300x169.png" alt="" width="320" height="180" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Castle - By Derme (Me)
    </td>
  </tr>
</table>

This is where it shines that I'm not all that creative, all the earth-shattering stuff going on around me and I'm building a 4 walled castle.

<table style="margin-left: auto; margin-right: auto; text-align: center;" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="text-align: center;">
      <a style="margin-left: auto; margin-right: auto;" href="http://derme.coffee/uploads/2011/10/Minecraft_NumCat.png"><img src="http://derme.coffee/uploads/2011/10/Minecraft_NumCat-300x169.png" alt="" width="320" height="180" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td style="text-align: center;">
      Nyan Cat - By Kyza98
    </td>
  </tr>
</table>

This has gotta be my personally favourite for today, not original, or creative, but it looks so colourful.

So there you have it, for those of you that are dedicated Minecrafters these creations were made;

  * In Creative mode (With mobs on)
  * Using NO mods of any sort
  * All creations were made in 48 hours or less

Other than Minecraft, it's hard to imagine isn't it? I've been working on my HTML5 scrolling shooter which should hopefully be done within the next couple days.