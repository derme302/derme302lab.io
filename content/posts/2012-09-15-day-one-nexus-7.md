---
id: 33
title: 'Day One: Nexus 7'
date: 2012-09-15T07:14:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2012/day-one-nexus-7/
permalink: /2012/09/day-one-nexus-7/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2012/09/day-one-nexus-7.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/1267023326865636304
categories:
  - Tech
tags:
  - nexus
---
I decided last week that I needed a tablet to test my upcoming Android app that I've been working on over the last few months. After writing several pro/con lists, I settled on Asus' Nexus 7.

The main reason I decided to go with the Nexus 7 for app development was that it is running the newest version of Android (4.1.1) which is brilliant for testing my app. Being the flagship Google Tablet I expect that it will get all the major revisions of Android almost as they are released.

<div>
  <div style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="http://derme.coffee/uploads/2012/09/google_nexus7.png"><img src="http://derme.coffee/uploads/2012/09/google_nexus7-300x206.png" alt="" width="320" height="220" border="0" /></a>
  </div>
  
  <div style="clear: both; text-align: center;">
  </div>
  
  <p>
    <!--more-->
    
    <br /> When I was looking at tablets in stores, 7&#8243; seemed way to small as my netbook is 11&#8243; and there never seems to be enough room on the screen. Once I'd sat down and used it for a few hours I actually understand why 7&#8243; is so much better than a 10&#8243; iPad or Samsung Galaxy Tab. 7&#8243; means I can hold it in one hand easily, leaving the other free to swipe and tap away.
  </p>
  
  <p>
    Weight wasn't something I had considered when looking for a tablet, but it's probably something I should of, weight dictates how many hours you can hold something for until it gets uncomfortable. I have a Kindle Touch with a case which weighs about 500g and I can hold it in one hand for about an hour to an hour and a half until it starts to irritate me, where as I can hold the Nexus 7 (340g) for about two to three hours.
  </p>
  
  <p>
    Heat is another thing you probably want to consider, while the Nexus 7 lacks HDMI out, USB ports, ect. I have found though that the tablets I tested got extremely hot near the port areas even when the ports were not being used. Personally I don't like holding a small volcano in my hand, so I'm really impressed how cool the Nexus stays even after a few hours usage.
  </p>
  
  <p>
    I only really have two disappointments with the Nexus, first is the lack of support for a SD Card or model with more storage, even with the 16GB model I'm already filling it up fairly fast. The second issue, which isn't major, is the lack of a physical home button. Though this is something that I'm getting used to, I just dislike giving up precious screen real estate.
  </p>
  
  <p>
    &nbsp;
  </p>
  
  <p>
    Overall I would give the Nexus 4 out of 5 hot potatoes and would recommend it to anyone looking to buy a tablet.
  </p>
  
  <div>
  </div>
  
  <div>
    <i><span style="font-size: x-small;">Image (Nexus 7): <a href="https://play.google.com/store/devices/details/Nexus_7_8GB?id=nexus_7_8gb&hl=en">Google Play Store</a></span></i>
  </div>
</div>