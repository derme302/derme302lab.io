---
id: 31
title: 'The Glass is Falling - Release'
date: 2012-12-23T05:38:00+00:00
author: Derme
layout: post
guid: http://derme.coffee/2012/the-glass-is-falling-release/
permalink: /2012/12/the-glass-is-falling-release/
blogger_blog:
  - derme.blogspot.com
blogger_author:
  - Derme
blogger_permalink:
  - /2012/12/the-glass-is-falling-release.html
blogger_internal:
  - /feeds/6224486094781961260/posts/default/472948916590812406
categories:
  - GameMaker
  - Games
---
&#8216;The Glass is Falling' is a game, well more mini-game, that I've been working on in the background for the last week. The goal of the game is simple, stop the glass from hitting the floor. It's available now on the <a href="http://steamcommunity.com/sharedfiles/filedetails/?id=115103698&searchtext=" target="_blank">GM:S Steam Workshop</a>, so if you want to have a play before reading on go ahead!

<div>
  <a href="http://derme.coffee/uploads/2012/12/r10_22-12-12.png"><img class="aligncenter" src="http://derme.coffee/uploads/2012/12/r10_22-12-12-224x300.png" alt="" width="238" height="320" border="0" /></a>
</div>

It was the first game that I designed & planned before I started development and I was extremely happy when the final product turned out like the concept art.

<div id="attachment_60" style="width: 233px" class="wp-caption alignleft">
  <a href="http://derme.coffee/uploads/2012/12/MockUp.png"><img class="wp-image-60 size-medium" src="http://derme.coffee/uploads/2012/12/MockUp-223x300.png" alt="MockUp" width="223" height="300" srcset="https://derme.coffee/uploads/2012/12/MockUp-223x300.png 223w, https://derme.coffee/uploads/2012/12/MockUp.png 477w" sizes="(max-width: 223px) 100vw, 223px" /></a>
  
  <p class="wp-caption-text">
    Concept Art
  </p>
</div>

<div id="attachment_59" style="width: 234px" class="wp-caption alignright">
  <a href="http://derme.coffee/uploads/2012/12/r8_16-12-12.png"><img class="wp-image-59 size-medium" src="http://derme.coffee/uploads/2012/12/r8_16-12-12-224x300.png" alt="r8_16-12-12" width="224" height="300" srcset="https://derme.coffee/uploads/2012/12/r8_16-12-12-224x300.png 224w, https://derme.coffee/uploads/2012/12/r8_16-12-12.png 480w" sizes="(max-width: 224px) 100vw, 224px" /></a>
  
  <p class="wp-caption-text">
    Final Build
  </p>
</div>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

<!--more-->

Only one feature was added that wasn't included in the plan and that is that in the final version there are two types of particles the red one (Symbolising Nitrogen) and the blue one (Symbolising Oxygen), this was to try and add a bit more depth into the game as the Oxygen particles have a greater force behind them.

<p style="text-align: left;">
  I'm happy with how the game turned out, expect for the fact that the glass always seems left-aligned, something I'll try to fix in a later version. Speaking of later versions, I've already started planning the next three revisions to the game.
</p>

<div>
  <div>
    <strong>Version 1.1:</strong>
  </div>
  
  <div>
    <ul>
      <li>
        Animate Glass Smash
      </li>
      <li>
        Polish the sprites some more
      </li>
      <li>
        Background Art
      </li>
      <li>
        Help Menu & Credits included in Game
      </li>
      <li>
        HTML5 Port
      </li>
      <li>
        Mac Port
      </li>
    </ul>
    
    <div>
    </div>
  </div>
  
  <div>
    <strong>Version 1.2:</strong>
  </div>
  
  <div>
    <ul>
      <li>
        Audio & Cool Sound Effects (Includes Background Music)
      </li>
      <li>
        Better Scaling
      </li>
      <li>
        Android Port
      </li>
    </ul>
  </div>
  
  <div>
  </div>
  
  <div>
    <strong>Version 1.3:</strong>
  </div>
  
  <div>
    <ul>
      <li>
        Online Highscores
      </li>
    </ul>
    
    <p>
      While I'm working on these I'll still be polishing the physics engine to try and give it a really nice feel, I also want to maybe add some power-ups (Hurricane maybe?) The 1.1 update isn't due until late January I'm going to take a break while I work on my main project and also another side project that I've been meaning to build a prototype for.
    </p>
  </div>
</div>

If you have a suggestion or idea, let me know in the comments.

Merry Christmas!